package tdgame.enemies;

import tdgame.enemies.Enemy.type;

public class Skeleton extends Enemy{
        private static String name = "Flying";
	private static double health;
	private static double speed;
	private static double armor;
	private static int reward;
        public static final int heigth=27;
        public static final int width=16;
        public int elemental;
    
	public Skeleton(int[][] Locations, double health, double speed, double armor, int reward, int damage){
            super(Locations, health, speed, armor, reward, damage, name, type.SKELETON);
            elemental=0;
        }
}