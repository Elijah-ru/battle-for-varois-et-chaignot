**To Do list for the 4th milestone :**



__1. Needs to be done__\
\
*-implement the end of a level (with either a win or a loss)*\
(Most of the needed stuff is already done, It just needs a bit of wrapping and some tests)\
\
*-implement the mechanism of elementals*\
(Needs a bit more tweaks. Some method rewriting should be done. dont forget Unit tests aswell)\
\
*-fix the AcquireTargets() method*\
(Turrets dont target the enemy which is the closest to the goal <=> the one with the greater distance traveled. Need some tests and some Unit tests. See related Issue)\
\
*-Investigate the bug of the first wave*\
(see related Issue)\
\
*-Other things not really specific :*\
Clean Code\
Unit Tests for the rest of the code\
\
__2. Additional features__\
\
*-Add more levels*\
(with path locations)\
\
*-Projectiles*\
\
*-Options Menu*\
(make possible to change difficulty ? could be done easily just by adding new values in the text files)\
\
*-Highscores Menu*\
(Not really interesting in with our choice of having finite levels. Would need an endless mode)\
\
*-Testing of the game*\
(Balance the values of the game)\
\
*-jar version*